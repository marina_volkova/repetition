package ru.vma.task8;

import java.util.Scanner;

public class Nullify {

    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
        for (int[] ints : matrix) {
            for (int anInt : ints)
                System.out.print(anInt + " ");
            System.out.println();
        }

        System.out.println("Введите число: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == n) matrix[i][j] = 0;
            }
        }
        for (int[] ints : matrix) {
            for (int anInt : ints)
                System.out.print(anInt + " ");
            System.out.println();
        }
    }
}

