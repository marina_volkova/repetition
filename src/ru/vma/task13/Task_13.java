package ru.vma.task13;

import java.util.Scanner;

public class Task_13 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символ: ");
        char c = scanner.next().charAt(0);
        String c1 = scanner.nextLine();

        if (Character.isDigit(c)) {
            System.out.println("Символ является числом!");
        } else if (Character.isLetter(c)) {
            System.out.println("Символ является буквой!");
        } else if (".,:;-".contains(c1)) {
            System.out.println("Символ является знаком пунктуации!");
        } else System.out.println("Символ не является ни буквой, ни цифрой, ни знаком пунктуации!");

    }
}
