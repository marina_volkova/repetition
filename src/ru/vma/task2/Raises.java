package ru.vma.task2;

import java.util.Scanner;

public class Raises {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] n = {20, 30, 40, 50, 60, 70, 80, 90, 100, 110};
        for (int j = 0; j < 10; j++)
            System.out.print(n[j] + " ");
        System.out.println();
        System.out.println("Ведите индекс ");
        int i = scanner.nextInt() - 1;
        newArr(i, n);

    }

    private static void newArr(int i, int[] n) {
        n[i] *= 1.1;
        for (int j = 0; j < 10; j++) {
            System.out.print(n[j] + " ");
        }
    }
}
