package ru.vma.task3;

import java.util.Scanner;

public class Converter {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        float r, k;
        System.out.println("Введите количество рублей ");
        r = reader.nextFloat();
        System.out.println("Введите курс ");
        k = reader.nextFloat();
        System.out.println("Получилось " + convert(r, k) + " евро");
    }

    public static float convert(float r, float k) {
        return r / k;
    }
}
