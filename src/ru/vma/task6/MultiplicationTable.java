package ru.vma.task6;

import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int t;
        System.out.println("Введите число ");
        t = reader.nextInt();
        for (int i = 1; i <= 10; i++)
            System.out.println(t + "*" + i + "=" + (t * i));
    }
}
