package ru.vma.task9;

import java.util.Scanner;

public class PalindromeNumber {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число:");
        int number = scanner.nextInt();
        if (isPalindrome(Integer.toString(number))) {
            System.out.println("Число является палиндромом.");
        }else {
            System.out.println("Число не является палиндромом.");
        }
    }

    private static boolean isPalindrome(String j) {
        for (int i = 0; i < j.length() / 2; i++) {
            if (j.charAt(i) != j.charAt(j.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
