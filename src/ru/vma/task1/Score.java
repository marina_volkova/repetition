package ru.vma.task1;

import java.util.Scanner;

public class Score {
    public static void main(String[] args) {
        char c;
        int i = 0;
        Scanner reader = new Scanner(System.in);
        c = reader.next().charAt(0);
        while (c != '.') {
            if (c == ' ')
                i++;
            c = reader.next().charAt(0);
        }
        System.out.println(i);
    }
}
