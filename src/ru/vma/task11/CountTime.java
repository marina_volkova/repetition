package ru.vma.task11;

import java.util.Scanner;

public class CountTime {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во дней: ");
        int d = scanner.nextInt();
        System.out.println("В " + d + " днях всего\n"

                + h(d) + " часов,\n"

                + m(d) + " минут,\n"

                + s(d) + " секунд.");
    }

    public static int h(int d) {
        return d * 24;
    }

    public static int m(int d) {
        return d * 1440;
    }

    public static int s(int d) {
        return d * 86400;
    }
}
